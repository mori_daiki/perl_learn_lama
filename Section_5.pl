#!/usr/bin/env perl
use strict;
use warnings;
use utf8;
use 5.010000;
use autodie;

=pod
print chomp( my $line = <STDIN> );
while(<STDIN>){
    print "I saw $_";
}
# foreachでループする場合、実行前に全ての入力を読み込んでしまうので注意せよ
=cut

my @array = qw/ tes test tst /;
print "@array\n"; # こう書くと、要素と要素の間に空白が入る

my $a = "a!";
my $b = "b!";
printf "Hello, %s and %s", $a, $b; # フォーマット指定文字

my @items = qw( wilma dino pebbles );
my $format = "The items are:\n".("%10s\n" x @items);
printf $format, @items;

say "pritn";
