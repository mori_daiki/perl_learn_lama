#!/usr/bin/env perl
use strict;
use warnings;
use utf8;
use 5.010000;
use autodie;
use Data::Dumper;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";

# indexは先頭から数える
my $stuff = "Hello, world";
my $where = index($stuff, "wor");
say $where; #=> 7

# rindexは末尾から数える
my $last_slash = rindex("/etc/passwd", "/");
say $last_slash; #=> 4

my $mineral = substr("Fred J. Flintstone", 8, 5);
say $mineral; #=> "Flint"

my $long = "some very very long string";
say index($long, "1"); #=> -1 ... 文字列が見つからなければ-1を返す
say index($long, "l"); #=> 15

my $right = substr($long, index($long, "l"));
say $right; #=> "long string"

