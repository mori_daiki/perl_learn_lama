#!/usr/bin/env perl
use strict;
use warnings;
use utf8;
use 5.010000;
use autodie;
use Data::Dumper;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";

my $original = "Fred ate 1 rib";
my $copy = $original;
$copy =~ s/\d+ ribs?/10 ribs/;
say $copy;

(my $poppy = $original) =~ s/\d+ ribs?/10 ribs/;
say $poppy;

# ///rは元の文字列は変更せず、置換の結果を値として返す
# ///gはすべての箇所変更

my $a = "I saw Barney with Fred.";
my $b = $a =~ s/(fred|barney)/\U$1/rgi; # iは大文字小文字を区別しない
say $b;
# \Uはすべて大文字、\Lはすべて小文字、\u, \lは最初の1文字だけ。これらを組み合わせることも可能

my @fields = split /:/, "abc:def:h:g:a";
say Dumper(@fields);
my $y = join "-", @fields;
say $y; #=> abc-def-h-g-a

